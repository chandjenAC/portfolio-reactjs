import React from "react";
import "./App.scss";
import Header from "./components/Header";
import FrontPage from "./components/FrontPage";
import Loader from "./components/Loader";

function App() {
  return (
    <div className="portfolio">
      <Loader />
      <Header />
      <FrontPage />
    </div>
  );
}

export default App;

// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

// export default App;
