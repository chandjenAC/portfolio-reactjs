import React, { Component } from "react";
import "../styles/header.scss";
class Header extends Component {
  render() {
    return (
      <div className="container">
        <div className="title-container">CJ</div>
        <div className="menu">M E N U</div>
      </div>
    );
  }
}

export default Header;
