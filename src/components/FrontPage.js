import React from "react";
import "../styles/frontPage.scss";

const FrontPage = () => {
  return (
    <div>
      <section className="home">
        <div className="hero-section">
          <div className="hero-title">
            Delivering super smooth UI and web experiences
          </div>
          <div className="hero-content">
            <p>
              This is where I write some catchy lines about myself in juust a
              paragraph or may be three sentences tops.
            </p>
          </div>
        </div>
      </section>
      <section className="experience1"> EXPERIENCE 1</section>
      <section className="experience2"> EXPERIENCE 2</section>{" "}
      <section className="experience3"> EXPERIENCE 3</section>
    </div>
  );
};

export default FrontPage;
